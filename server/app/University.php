<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
  protected $fillable = ['name'];


  public function student()
  {
      return $this->hasMany('Student');
  }

  public function program()
  {
      return $this->belongsToMany('Program');
  }
}
