<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $fillable = ['username','password','email','photo','first_name','second_name','first_surname','second_surname','description'];

  public function university()
  {
      return $this->belongsTo('App\University');
  }
  public function program()
  {
      return $this->belongsTo('Program');
  }
  public function emphasis()
  {
      return $this->belongsTo('Emphasis');
  }

  public function semester()
  {
      return $this->belongsTo('Semester');
  }

    public function project()
  {
      return $this->belongsToMany('Project');
  }


}

