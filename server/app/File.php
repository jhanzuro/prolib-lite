<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
  protected $fillable = ['identifier'];

  public function type()
  {
      return $this->belongsTo('Type');
  }

  public function project()
  {
      return $this->belongsTo('Project');
  }
}
