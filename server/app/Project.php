<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $fillable = ['name','description'];

  public function file()
  {
      return $this->hasMany('File');
  }
  public function student()
  {
      return $this->belongsToMany('Student');
  }

  public function tag()
  {
      return $this->belongsToMany('Tag');
  }

  public function group()
  {
      return $this->belongsToMany('Group');
  }
}
