<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
  protected $fillable = ['name'];

  public function group()
  {
      return $this->hasMany('Group');
  }

  public function program()
  {
      return $this->belongsToMany('Program');
  }
}
