<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emphasis;

class EmphasisController extends Controller
{
  public function index()
  {
      return Emphasis::all();
  }

  public function show(Emphasis $emphasis)
  {
      return $emphasis;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'name' => 'required|unique:emphases',
      ]);

      $emphasis = Emphasis::create($request->all());

      return response()->json($emphasis, 201);
  }

  public function update(Request $request, Emphasis $emphasis)
  {
      $emphasis->update($request->all());

      return response()->json($emphasis, 200);
  }

  public function delete(Emphasis $emphasis)
  {
      $emphasis->delete();

      return response()->json(null, 204);
  }
}
