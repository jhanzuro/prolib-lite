<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Semester;

class SemesterController extends Controller
{
  public function index()
  {
      return Semester::all();
  }

  public function show(Semester $semester)
  {
      return $semester;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'semester' => 'required|unique:semesters',
      ]);

      $semester = Semester::create($request->all());

      return response()->json($semester, 201);
  }

  public function update(Request $request, Semester $semester)
  {
      $semester->update($request->all());

      return response()->json($semester, 200);
  }

  public function delete(Semester $semester)
  {
      $semester->delete();

      return response()->json(null, 204);
  }
}
