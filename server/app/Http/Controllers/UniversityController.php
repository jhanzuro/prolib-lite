<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\University;

class UniversityController extends Controller
{
  public function index()
  {
      return University::all();
  }

  public function show(University $university)
  {
      return $university;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'name' => 'required|unique:universities',

      ]);

      $university = University::create($request->all());

      return response()->json($university, 201);
  }

  public function update(Request $request, University $university)
  {
      $university->update($request->all());

      return response()->json($university, 200);
  }

  public function delete(University $university)
  {
      $university->delete();

      return response()->json(null, 204);
  }
}
