<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
  public function index()
  {
      return Type::all();
  }

  public function show(Type $type)
  {
      return $type;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'name' => 'required|unique:types',

      ]);

      $type = Type::create($request->all());

      return response()->json($type, 201);
  }

  public function update(Request $request, Type $type)
  {
      $type->update($request->all());

      return response()->json($type, 200);
  }

  public function delete(Type $type)
  {
      $type->delete();

      return response()->json(null, 204);
  }
}
