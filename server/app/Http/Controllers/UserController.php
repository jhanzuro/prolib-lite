<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
  public function index()
  {
      return User::all();
  }

  public function show(User $user)
  {
      return $user;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'username' => 'required|unique:users',
        'password' => 'required:users',
        'first_name' => 'required:users',
        'last_name' => 'required:users',
        'email' => 'required|unique:users',

      ]);

      $user = User::create($request->all());

      return response()->json($user, 201);
  }

  public function update(Request $request, User $user)
  {
      $user->update($request->all());

      return response()->json($user, 200);
  }

  public function delete(User $user)
  {
      $user->delete();

      return response()->json(null, 204);
  }
}
