<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;

class PeriodController extends Controller
{
  public function index()
  {
      return Period::all();
  }

  public function show(Period $period)
  {
      return $period;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'date' => 'required|unique:periods',

      ]);

      $period = Period::create($request->all());

      return response()->json($period, 201);
  }

  public function update(Request $request, Period $period)
  {
      $period->update($request->all());

      return response()->json($period, 200);
  }

  public function delete(Period $period)
  {
      $period->delete();

      return response()->json(null, 204);
  }
}
