<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
  public function index()
  {
      return State::all();
  }

  public function show(State $state)
  {
      return $state;
  }

  public function store(Request $request)
  {
      $this->validate($request, [
        'name' => 'required|unique:states',

      ]);
      $state = State::create($request->all());

      return response()->json($state, 201);
  }

  public function update(Request $request, State $state)
  {
      $state->update($request->all());

      return response()->json($state, 200);
  }

  public function delete(State $state)
  {
      $state->delete();

      return response()->json(null, 204);
  }
}
