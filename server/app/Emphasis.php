<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emphasis extends Model
{
  protected $fillable = ['name'];

  public function program()
  {
      return $this->belongsTo('Program');
  }
    public function student()
  {
      return $this->hasMany('Student');
  }

}
