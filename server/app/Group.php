<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
  public function state()
  {
      return $this->belongsTo('State');
  }

  public function period()
  {
      return $this->belongsTo('Period');
  }

  public function course()
  {
      return $this->belongsTo('Course');
  }

  public function project()
  {
      return $this->belongsToMany('Project');
  }
}
