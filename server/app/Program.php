<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
  protected $fillable = ['name'];

  public function emphasis()
  {
      return $this->hasMany('Emphasis');
  }

  public function student()
  {
      return $this->hasMany('Student');
  }

  public function university()
  {
      return $this->belongsToMany('University');
  }

  public function course()
  {
      return $this->belongsToMany('Course');
  }

}
