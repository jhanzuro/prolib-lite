<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
  protected $fillable = ['date'];

  public function group()
  {
      return $this->hasMany('Group');
  }

}
