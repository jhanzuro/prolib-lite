<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
**Basic Routes for a RESTful service:
**Route::get($uri, $callback); -> get
**Route::post($uri, $callback); -> insert
**Route::put($uri, $callback); -> update
**Route::delete($uri, $callback); -> delete
**
*/

/* Types -> file types routes */
Route::get('types', 'TypeController@index');
Route::get('types/{type}', 'TypeController@show');
Route::post('types','TypeController@store');
Route::put('types/{type}','TypeController@update');
Route::delete('types/{type}', 'TypeController@delete');

/* states -> group states routes */
Route::get('states', 'StateController@index');
Route::get('states/{state}', 'StateController@show');
Route::post('states','StateController@store');
Route::put('states/{state}','StateController@update');
Route::delete('states/{state}', 'StateController@delete');

/* projects -> projects routes */
Route::get('projects', 'ProjectController@index');
Route::get('projects/{project}', 'ProjectController@show');
Route::post('projects','ProjectController@store');
Route::put('projects/{project}','ProjectController@update');
Route::delete('projects/{project}', 'ProjectController@delete');

/* Courses -> University -> Program -> Courses (materias) routes */
Route::get('courses', 'CourseController@index');
Route::get('courses/{course}', 'CourseController@show');
Route::post('courses','CourseController@store');
Route::put('courses/{course}','CourseController@update');
Route::delete('courses/{course}', 'CourseController@delete');

/* University -> University -> Program -> Courses (materias) routes */
Route::get('universities', 'UniversityController@index');
Route::get('universities/{university}', 'UniversityController@show');
Route::post('universities','UniversityController@store');
Route::put('universities/{university}','UniversityController@update');
Route::delete('universities/{university}', 'UniversityController@delete');

/* Program -> University -> Program -> Courses (materias) routes */
Route::get('programs', 'ProgramController@index');
Route::get('programs/{program}', 'ProgramController@show');
Route::post('programs','ProgramController@store');
Route::put('programs/{program}','ProgramController@update');
Route::delete('programs/{program}', 'ProgramController@delete');

/* User -> a user is an admin or superadmin */
Route::get('users', 'UserController@index');
Route::get('users/{user}', 'UserController@show');
Route::post('users','UserController@store');
Route::put('users/{user}','UserController@update');
Route::delete('users/{user}', 'UserController@delete');

/* Files -> There are the resources that compose a project */
Route::get('files', 'FileController@index');
Route::get('files/{file}', 'FileController@show');
Route::post('files','FileController@store');
Route::put('files/{file}','FileController@update');
Route::delete('files/{file}', 'FileController@delete');

/* Emphases -> Emphasis in singular, The career empashis depending university and program */
Route::get('emphases', 'EmphasisController@index');
Route::get('emphases/{emphasis}', 'EmphasisController@show');
Route::post('emphases','EmphasisController@store');
Route::put('emphases/{emphasis}','EmphasisController@update');
Route::delete('emphases/{emphasis}', 'EmphasisController@delete');

/* Files -> There are the resources that compose a project */
Route::get('files', 'FileController@index');
Route::get('files/{file}', 'FileController@show');
Route::post('files','FileController@store');
Route::put('files/{file}','FileController@update');
Route::delete('files/{file}', 'FileController@delete');

/* Students -> students */
Route::get('students', 'StudentController@index');
Route::get('students/{student}', 'StudentController@show');
Route::post('students','StudentController@store');
Route::put('students/{student}','StudentController@update');
Route::delete('students/{student}', 'StudentController@delete');

Route::get('students/university/{id}','StudentController@university');

/* Groups -> a group is conformed by materia and academic period */
Route::get('groups', 'GroupController@index');
Route::get('groups/{group}', 'GroupController@show');
Route::post('groups','GroupController@store');
Route::put('groups/{group}','GroupController@update');
Route::delete('groups/{group}', 'GroupController@delete');

/* Tags -> a tag  */
Route::get('tags', 'TagController@index');
Route::get('tags/{tag}', 'TagController@show');
Route::post('tags','TagController@store');
Route::put('tags/{tag}','TagController@update');
Route::delete('tags/{tag}', 'TagController@delete');

/* Periods -> a period  */
Route::get('periods', 'PeriodController@index');
Route::get('periods/{period}', 'PeriodController@show');
Route::post('periods','PeriodController@store');
Route::put('periods/{period}','PeriodController@update');
Route::delete('periods/{period}', 'PeriodController@delete');
