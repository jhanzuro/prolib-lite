<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'username' => $faker->unique()->userName,
        'photo' => $faker->imageUrl($width = 640, $height = 480),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'email' => $faker->unique()->safeEmail,
        'first_name' => $faker->firstName,
        'second_name' => $faker->firstName,
        'first_surname' => $faker->lastName,
        'second_surname' => $faker->lastName,
        'description' => $faker->text($maxNbChars = 300),
        'semester_id' => $faker->numberBetween($min = 1, $max = 10),
        'university_id' => $faker->numberBetween($min = 1, $max = 2),
        'program_id' => $faker->numberBetween($min = 1, $max = 1),
        'emphasis_id' => $faker->numberBetween($min = 1, $max = 1)
    ];
});
