<?php

use Illuminate\Database\Seeder;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('programs')->insert([
        ['name' => 'Ingeniería Multimedia'],
        ['name' => 'Ingeniería de Sistemas'],
        ['name' => 'Arquitectura']
      ]);
    }
}
