<?php

use Illuminate\Database\Seeder;

class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('semesters')->insert([
        ['semester' => '1'],
        ['semester' => '2'],
        ['semester' => '3'],
        ['semester' => '4'],
        ['semester' => '5'],
        ['semester' => '6'],
        ['semester' => '7'],
        ['semester' => '8'],
        ['semester' => '9'],
        ['semester' => '10']
      ]);
    }
}
