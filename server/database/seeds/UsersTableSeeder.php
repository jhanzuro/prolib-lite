<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'username' => 'sadmin',
          'password' => bcrypt('123'),
          'first_name' => 'Super',
          'last_name' => 'Admin',
          'email' => 'sadmin@correo.com',
        ],
        [
          'username' => 'acnicolasdc',
          'password' => bcrypt('root'),
          'first_name' => 'Nicolás',
          'last_name' => 'Reyes',
          'email' => 'nicolasbillwilliams@gmail.com',
        ],
        [
          'username' => 'jhanzuro',
          'password' => bcrypt('admin'),
          'first_name' => 'Jhonatan',
          'last_name' => 'Zuluaga',
          'email' => 'jhonatanzulr@hotmail.com',
        ]
      ]);
    }
}
