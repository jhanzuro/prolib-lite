<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
        SemesterTableSeeder::class,
        TypesTableSeeder::class,
        StatesTableSeeder::class,
        ProjectsTableSeeder::class,
        TagsTableSeeder::class,
        CoursesTableSeeder::class,
        UniversitiesTableSeeder::class,
        ProgramsTableSeeder::class,
        UsersTableSeeder::class,
        FilesTableSeeder::class,
        PeriodsTableSeeder::class,
        EmphasesTableSeeder::class,
        StudentsTableSeeder::class,
        GroupsTableSeeder::class
      ]);
    }
}
