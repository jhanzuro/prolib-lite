<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('courses')->insert([
        ['name' => 'Computación gráfica'],
        ['name' => 'Procesamiento de imágenes'],
        ['name' => 'Introducción a la programación']
      ]);
    }
}
