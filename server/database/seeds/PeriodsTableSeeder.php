<?php

use Illuminate\Database\Seeder;

class PeriodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('periods')->insert([
        ['date' => '2017-1'],
        ['date' => '2017-2'],
        ['date' => '2018-1']
      ]);
    }
}
