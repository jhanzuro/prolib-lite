<?php

use Illuminate\Database\Seeder;

class EmphasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('emphases')->insert([
        [
          'name' => 'Desarrollo web',
          'program_id' => '1'
        ],
        [
          'name' => 'Desarrollo de videojuegos',
          'program_id' => '1'
        ],
        [
          'name' => 'Arquitectura de software',
          'program_id' => '2'
        ],
        [
          'name' => 'Diseño de interiores',
          'program_id' => '3'
        ]
      ]);
    }
}
