// Dependencies
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';

//Routes
import AppRoutes from './routes';

// Assets
import './index.css';
import './components/global/assets/fontello-1f94dadc/css/fontello.css';

render(
  <Router>
    <AppRoutes />
  </Router>
  ,
  document.getElementById('root')
);
registerServiceWorker();
