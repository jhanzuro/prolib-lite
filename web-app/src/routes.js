// Dependencies
import React from 'react';
import { Route, Switch } from 'react-router-dom';

//Components
import App from './components/App';
import Profile from './components/profile';

const AppRoutes = () =>
  <App>
      <Switch>
        <Route path='/profile' component={Profile} />
      </Switch>
  </App>

export default AppRoutes;
