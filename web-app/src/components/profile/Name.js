import React, { Component } from 'react';

class Name extends Component {
  render() {
    return (
      <div className="profile_info">
        <div className="profile_name">
          <h1>{this.props.name}</h1>
        </div>
        <div className="profile_username">
          <h2>{this.props.username}</h2>
        </div>
      </div>
    );
  }
}

export default Name;
