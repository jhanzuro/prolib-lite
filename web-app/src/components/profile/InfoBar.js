import React, { Component } from 'react';
import InfoBlock from './InfoBlock';

class InfoBar extends Component {
  render() {
    let infoBlocks = this.props.infoBlocks;
    let listInfoBlocks = infoBlocks.map((element) =>
      <InfoBlock title={element.title} number={element.number} key={element.title} />
    );

    return (
      <div className="profile_est">
        <div className="button-block">
        </div>
        <div className="container">
          {listInfoBlocks}
        </div>
        <div className="button-block">
          <button><b>Edit Profile</b></button>
        </div>
      </div>
    );
  }
}

export default InfoBar;
