import React, { Component } from 'react';

class Info extends Component {

  render() {

    return (
      <div className="My_info">
        <div className="aside_profile"><h1>My Profile</h1></div>
        <div className="aside_email"><h5>{this.props.info.email}</h5></div>
        <div className="aside_descrip"><p>{this.props.info.description}</p></div>
        <div className="aside_univer"><h5>{this.props.info.university}</h5></div>
        <div className="aside_title"><h5>{this.props.info.program}</h5></div>
        <div className="aside_enfasis"><h5>{this.props.info.emphasis}</h5></div>
      </div>
    );
  }
}

export default Info;
