import React, { Component } from 'react';
import Picture from './Picture';
import Name from './Name';
import InfoBar from './InfoBar';
import photo from './assets/so.jpg';

class Banner extends Component {

  render() {

    let student = this.props.student;

    return (
      <div className="banner">
        <Picture src={photo}/>
        <Name name={student.name} username={student.username} />
        <InfoBar infoBlocks={student.infoBar} />
      </div>
    );
  }
}

export default Banner;
