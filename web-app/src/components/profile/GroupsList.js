import React, { Component } from 'react';
import GroupBlock from './GroupBlock';

class GroupsList extends Component {

  render() {

    let groups = this.props.groups;
    let groupsList = groups.map((element) =>
      <GroupBlock name={element.name} members={element.members} key={element.name} />
    );

    return (
      <div className="my_groups">
        <div className="my_groups_profile"><h1>My Groups</h1></div>
        {groupsList}
      </div>
    );
  }
}

export default GroupsList;
