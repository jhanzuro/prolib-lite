import React, { Component } from 'react';

class GroupBlock extends Component {

  render() {

    return (
      <div className="groups_block">
        <div className="gruop_icon"><div className="gruop_icon_circle"></div></div>
        <div className="group_info">
          <div className="group_info_title"><h1>{this.props.name}</h1></div>
          <div className="group_info_integra"><h5 className="icon-comment-2"></h5><p><b>{this.props.members} Integrantes</b></p></div>
        </div>
      </div>
    );
  }
}

export default GroupBlock;
