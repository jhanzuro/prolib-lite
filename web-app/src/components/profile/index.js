import React, { Component } from 'react';
import Banner from './Banner';
import Aside from './Aside';
import ProjectCard from '../global/Cards/ProjectCard';

class Profile extends Component {
  render() {

    const student = {
      name: 'Nicolas Reyes',
      username: '@acnicolasdc',
      info: {
        email: 'nicolaframe@gmail.com',
        description: 'Mi doctrina no es un dogma, es una guira para la acccion. Canserbero 2012, no se muere quien se va, solo se muere quien se olvida. si la vida nos da una oportunidad, trabajemos sobre ella.  <br>Web Developer.',
        university: 'Universidad de San Buenaventura',
        program: 'Ingeniería Multimedia',
        emphasis: 'Desarrollo Web'
      },
      infoBar: [
        {title: 'Likes', 'number': 300},
        {title: 'Comments', 'number': 100},
        {title: 'Projects', 'number': 20}
      ],
      groups: [
        {name: 'Web avanzado II', members: 12},
        {name: 'Computación gráfica', members: 24},
        {name: 'Bases de datos II', members: 30},
        {name: 'Arquitectura', members: 5}
      ],
      projects: [
        {
          name: 'Zulishop',
          description: 'Es una tienda virtual, desarrollada con el framework redFox junto con la integracion de tecologias como PHP, JS y HTML.',
          image: './images/amazon.PNG',
          members: [
            {name: 'Nicolas Reyes'},
            {name: 'Carlos Cortez'},
            {name: 'Jhonatan Zuluaga'}
          ],
          likes: 100,
          comments: 200
        },
        {
          name: 'Aguantapp',
          description: 'Aplicacion web movil con estructura MVC que permite encontrar pareja mediante el uso de GPS.',
          image: './images/tider.png',
          members: [
            {name: 'Nicolas Reyes'},
            {name: 'Juan Pablo Abadía'}
          ],
          likes: 100,
          comments: 200
        }
      ]
    };

    let projects = student.projects;
    let projectsList = projects.map((element) =>
      <ProjectCard project={element} key={element.name} />
    );

    return (
      <div className="Profile">
        <Banner student={student} />
        <section className="profile">
          <div className="profile_wrapper">
            <Aside info={student.info} groups={student.groups} />
            <div className="project_list">
              {projectsList}
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Profile;
