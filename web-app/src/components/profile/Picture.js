import React, { Component } from 'react';

class Picture extends Component {
  render() {
    return (
      <div className="profile_pic">
        <div className="sub_profile_pic">
          <div className="profile_pic_foto">
            <img src={this.props.src} alt="Student" />
          </div>
        </div>
      </div>
    );
  }
}

export default Picture;
