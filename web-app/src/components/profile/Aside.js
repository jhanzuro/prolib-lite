import React, { Component } from 'react';
import Info from './Info';
import GroupsList from './GroupsList';

class AsideInfo extends Component {

  render() {

    return (
      <aside>
        <Info info={this.props.info} />
        <GroupsList groups={this.props.groups} />
      </aside>
    );
  }
}

export default AsideInfo;
