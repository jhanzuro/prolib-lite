import React, { Component } from 'react';

class InfoBlock extends Component {
  render() {
    return (
      <div className="profile_est_block">
        <div className="profile_est_block_inf title"><h3><b>{this.props.title}</b></h3></div>
        <div className="profile_est_block_inf"><p>{this.props.number}</p></div>
      </div>
    );
  }
}

export default InfoBlock;
