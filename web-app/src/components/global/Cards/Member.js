import React, { Component } from 'react';

class Member extends Component {

  render() {

    return (
      <div className="block_group"><div className="circle_group"></div><p><b>{this.props.name}</b></p></div>
    );
  }
}

export default Member;
