import React, { Component } from 'react';
import Info from './Info';
import Image from './Image';
import SocialButtons from './SocialButtons';

class ProjectCard extends Component {

  render() {

    let project = this.props.project;

    return (
      <div className="target_b">
        <Info name={project.name} description={project.description} members={project.members} />
        <Image src={project.image} />
        <SocialButtons likes={project.likes} comments={project.comments} />
      </div>
    );
  }
}

export default ProjectCard;
