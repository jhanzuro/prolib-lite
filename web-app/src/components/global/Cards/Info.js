import React, { Component } from 'react';
import Member from './Member';

class Info extends Component {

  render() {

    let project = this.props;

    let members = project.members;
    let membersList = members.map((element) =>
      <Member name={element.name} key={element.name} />
    );

    return (
      <div className="header_target">
        <div className="header_target_title"><h1>{project.name}</h1></div>
        <div className="header_target_desc"><p>{project.description}</p></div>
        <div className="header_target_grou">
          {membersList}
        </div>
      </div>
    );
  }
}

export default Info;
