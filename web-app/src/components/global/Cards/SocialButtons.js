import React, { Component } from 'react';

class SocialButtons extends Component {

  render() {

    let project = this.props;

    return (
      <div className="footer_target">
        <div className="footer_target_wrapper">
          <div className="footer_target_block">
            <div className="block_link"><h5 className="icon-thumbs-up"></h5><p><b>{project.likes}</b></p></div>
            <div className="block_link"><h5 className="icon-comment-2"></h5><p><b>{project.comments}</b></p></div>
          </div>
        </div>
      </div>
    );
  }
}

export default SocialButtons;
