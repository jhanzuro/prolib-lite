import React, { Component } from 'react';

class Image extends Component {

  render() {

    return (
      <div className="body_target">
        <div className="adpta_img">
          <img src={this.props.src} alt="Project" />
        </div>
      </div>
    );
  }
}

export default Image;
