import React, { Component } from 'react';
import logo from './assets/logo.png';
import loginForm from '../login/LoginForm';

class Navbar extends Component {
  constructor() {
    super();
    this.state = {
      show: false
    }
  }

  render() {
    let that = this;

    function showHide () {
      that.setState((prevState) => ({
        show: !prevState.show
      }));
    }

    console.log(this.state.show);

    return (
      <nav>
        <div className="navbar-container">
          <div className="navbar-rigth-container">
            <img src={logo} alt="" className="navbar-logo"/>
            <h4 className="navbar-title">Prolib</h4>
          </div>
          <div className="navbar-left-container">
            <form className="search-container">
              <input type="text" id="search-bar" placeholder="Search" />
              <a href="#"><img className="search-icon" src="http://www.endlessicons.com/wp-content/uploads/2012/12/search-icon.png" alt="Search icon" /></a>
            </form>
            <span id="login-button" onClick={showHide}>¿Tienes una cuenta?<strong> iniciar sesión</strong></span>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
