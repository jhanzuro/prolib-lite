// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Navbar from './global/Navbar';

// Assets
import './App.css';
import Content from './Content';

class App extends Component {
  static PropTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const { children } = this.props;

    return (
      <div className="App">
        <header>
          <Navbar />
        </header>
        <Content body={children} />
      </div>
    );
  }
}

export default App;
